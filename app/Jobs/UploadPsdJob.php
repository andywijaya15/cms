<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use App\Enums\ProcessNameEnum;
use App\Models\PlanSewingDate;
use App\Actions\CreateUserAction;
use Illuminate\Support\Facades\DB;
use App\Actions\CreateUserLogAction;
use App\Events\PsdCreated;
use App\Events\PsdCreatedEvent;
use App\Events\ReloadPsdTable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Spatie\SimpleExcel\SimpleExcelReader;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadPsdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        protected string $fileName,
        protected int $userId
    ) {
    }

    /**
     * Execute the job.
     */
    public function handle(CreateUserLogAction $createUserLogAction): void
    {
        $path = storage_path('app/upload_psd/');
        $rows = SimpleExcelReader::create("{$path}/{$this->fileName}")
            ->headerOnRow(0)
            ->fromSheetName('active')
            ->getRows();
        foreach ($rows as $each) {
            $poBuyer = strtoupper($each['po_buyer'] ?? '');
            if (!$poBuyer) {
                continue;
            }
            if (!$each['psd']) {
                $createUserLogAction($this->userId, ProcessNameEnum::UPLOAD_PSD, "FILL PSD FOR PO BUYER : $poBuyer", true);
                continue;
            }
            $psd = Carbon::parse($each['psd']);
            $yearsDifference = $psd->diffInYears(now());
            if ($yearsDifference > 3) {
                $createUserLogAction($this->userId, ProcessNameEnum::UPLOAD_PSD, "PSD FOR $poBuyer MORE THAN 3 YEARS", true);
                continue;
            }
            $isPoBuyerExists = DB::connection('replication_bima_live')
                ->table('mid_master_po_buyer_v as mmpbv')
                ->where('mmpbv.po_buyer', $poBuyer)
                ->exists();
            if (!$isPoBuyerExists) {
                $createUserLogAction($this->userId, ProcessNameEnum::UPLOAD_PSD, "PO BUYER $poBuyer NOT FOUND IN ERP", true);
                continue;
            }
            DB::beginTransaction();
            PlanSewingDate::query()
                ->where('po_buyer', $poBuyer)
                ->where('is_active', true)
                ->update([
                    'is_active' => false,
                    'updated_by' => $this->userId
                ]);
            PlanSewingDate::query()
                ->create([
                    'created_by' => $this->userId,
                    'updated_by' => $this->userId,
                    'po_buyer' => $poBuyer,
                    'psd_date' => $psd,
                ]);
            DB::commit();
            PsdCreatedEvent::dispatch();

            $createUserLogAction($this->userId, ProcessNameEnum::UPLOAD_PSD, "UPLOAD PSD FOR PO BUYER $poBuyer SUCCESS");
        }
    }
}
