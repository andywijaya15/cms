<?php

namespace App\Console\Commands;

use App\Actions\CreateUserLogAction;
use App\Enums\ProcessNameEnum;
use App\Events\PsdCreated;
use App\Models\PlanSewingDate;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class trial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(CreateUserLogAction $createUserLogAction)
    {
        $createUserLogAction(
            1,
            ProcessNameEnum::UPLOAD_PSD,
            'test'
        );
    }
}
