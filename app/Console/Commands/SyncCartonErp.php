<?php

namespace App\Console\Commands;

use App\Models\Carton;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncCartonErp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-carton-erp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Master Carton ERP';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            DB::table('aw_master_carton_erp')
                ->cursor()
                ->each(function ($each) {
                    echo "Sync $each->item_id" . PHP_EOL;
                    $isExists = Carton::query()
                        ->where('item_id', $each->item_id)
                        ->exists();
                    if ($isExists) {
                        return;
                    }
                    DB::beginTransaction();
                    Carton::query()
                        ->create([
                            'item_id' => $each->item_id,
                            'item_code' => $each->item_code,
                            'item_desc' => $each->item_desc,
                            'category' => $each->category,
                            'uom' => $each->uom,
                            'color' => $each->color,
                            'upc' => $each->upc,
                            'width' => $each->width,
                            'is_recycle' => $each->is_recycle,
                            'brand' => $each->brand
                        ]);
                    DB::commit();
                });
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            DB::rollBack();
        }
    }
}
