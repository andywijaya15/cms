<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Js;
use Illuminate\Support\ServiceProvider;
use Livewire\Component;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->bootMacros();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /** @var \Illuminate\Foundation\Application $app */
        if (!$this->app->runningInConsole()) {
            Model::shouldBeStrict();
        };
    }

    public function bootMacros(): void
    {
        Component::macro('toast', function (string $text, string $type = 'success') {
            /** @var \Livewire\Component $this*/
            $this->js(<<<JS
                Swal.fire({
                    text: '{$text}',
                    icon: '{$type}',
                    toast: true,
                    showConfirmButton: false,
                    position: 'top',
                    timer: 2000
                });
            JS);
        });
    }
}
