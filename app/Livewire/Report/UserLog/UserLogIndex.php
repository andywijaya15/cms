<?php

namespace App\Livewire\Report\UserLog;

use App\Models\User;
use App\Models\UserLog;
use Livewire\Component;
use Livewire\WithPagination;
use App\Enums\ProcessNameEnum;
use Livewire\Attributes\Title;

#[Title('USER LOG')]
class UserLogIndex extends Component
{
    use WithPagination;
    public User $user;
    public string $search = '';
    public int $limitPerPage = 10;
    public ProcessNameEnum $processNameEnum = ProcessNameEnum::ALL;

    public function getListeners()
    {
        $this->user = auth()->user();
        return [
            "echo:App.Models.User.{$this->user->id},ReloadNotificationEvent" => 'updateList',
        ];
    }

    public function updateList()
    {
    }

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.report.user-log.user-log-index', [
            'user_logs' => UserLog::query()
                ->where('user_id', auth()->user()->id)
                ->when($this->processNameEnum != 0, fn ($q) => $q->where('process_name', $this->processNameEnum))
                ->whereAny(
                    [
                        'msg',
                    ],
                    'ILIKE',
                    "%$this->search%"
                )
                ->simplePaginate($this->limitPerPage),
            'process_names' => ProcessNameEnum::cases()
        ]);
    }
}
