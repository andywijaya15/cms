<?php

namespace App\Livewire\Report\AllocationCarton;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Livewire\Attributes\Title;

#[Title('ALLOCATION CARTON')]
class AllocationCartonIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.report.allocation-carton.allocation-carton-index', [
            'allocation_cartons' => DB::connection('fg_dev')
                ->table('aw_allocation_carton')
                ->when($this->search, fn ($q) => $q->whereAny(
                    [
                        'plan_ref_number',
                        'po_buyers',
                        'item_code',
                    ],
                    'ILIKE',
                    "%$this->search%"
                ))
                ->whereNotNull('item_id')
                ->orderBy('created_at', 'DESC')
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
