<?php

namespace App\Livewire\Master\Carton;

use App\Models\Carton;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;

#[Title('CARTON')]
class CartonIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.master.carton.carton-index', [
            'cartons' => Carton::query()
                ->whereAny(
                    [
                        'item_id',
                        'item_code',
                        'width',
                        'brand'
                    ],
                    'ILIKE',
                    "%$this->search%"
                )
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
