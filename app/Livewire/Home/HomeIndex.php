<?php

namespace App\Livewire\Home;

use Livewire\Attributes\Title;
use Livewire\Component;

#[Title('HOME')]
class HomeIndex extends Component
{
    public function render()
    {
        return view('livewire.home.home-index');
    }
}
