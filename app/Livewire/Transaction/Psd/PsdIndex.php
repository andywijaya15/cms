<?php

namespace App\Livewire\Transaction\Psd;

use App\Jobs\UploadPsdJob;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use App\Models\PlanSewingDate;
use Illuminate\Http\Response;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Spatie\SimpleExcel\SimpleExcelWriter;

#[Title('PSD')]
class PsdIndex extends Component
{
    use WithPagination, WithFileUploads;
    public string $search = '';
    public int $limitPerPage = 10;

    #[Validate('required')]
    public $excel = null;

    public $rand;

    public function getListeners()
    {
        return [
            "echo:PSD,PsdCreatedEvent" => 'updateList',
        ];
    }

    public function updateList()
    {
    }

    public function resetForm()
    {
        $this->rand++;
        $this->reset();
    }

    public function doSearch()
    {
        $this->resetPage();
    }

    public function downloadTemplate()
    {
        $time = now()->toDateTimeString();
        $fileName = "template-psd-$time.xlsx";
        return response()
            ->streamDownload(function () use ($fileName) {
                SimpleExcelWriter::streamDownload($fileName)
                    ->nameCurrentSheet('active')
                    ->addRow([
                        'po_buyer' => '',
                        'psd' => ''
                    ])
                    ->addNewSheetAndMakeItCurrent('example')
                    ->addRow([
                        'po_buyer' => '10130330797-X',
                        'psd' => now()->format('Y-m-d')
                    ])
                    ->close();
            }, $fileName);
    }

    public function uploadPsd()
    {
        try {
            $this->validate();
            if ($this->excel->extension() != 'xlsx') {
                throw new \Exception('Wrong file format!', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $userUpload = auth()->user()->name;
            $now = now()->toDateTimeString();
            $fileName = "UPLOAD_PSD $userUpload $now." . $this->excel->extension();
            $this->excel->storeAs(path: 'upload_psd', name: $fileName);
            UploadPsdJob::dispatch($fileName, auth()->user()->id)->onQueue('upload-psd');
            $this->toast('Upload Success');
            $this->resetForm();
        } catch (\Exception $e) {
            $this->toast($e->getMessage(), 'error');
        }
    }

    public function downloadReport()
    {
        $time = now()->toDateTimeString();
        $fileName = "report-psd-$time.xlsx";
        return response()
            ->streamDownload(function () use ($fileName) {
                $writer = SimpleExcelWriter::streamDownload($fileName)
                    ->nameCurrentSheet('active')
                    ->addHeader([
                        'po_buyer',
                        'psd',
                        'last_updated_by'
                    ]);
                $psds = PlanSewingDate::query()
                    ->with('createdBy')
                    ->where('is_active', true)
                    ->cursor();
                $counter = 0;
                foreach ($psds as $row) {
                    $counter = $counter + 1;
                    $writer->addRow([
                        $row->po_buyer,
                        $row->psd_date->format('d-M-Y'),
                        $row->createdBy->name
                    ]);
                    if ($counter > 1000) {
                        flush();
                        $counter = 0;
                    }
                }

                $writer->close();
            }, $fileName);
    }

    public function render()
    {
        return view('livewire.transaction.psd.psd-index', [
            'psds' => PlanSewingDate::query()
                ->with('createdBy')
                ->whereAny(
                    [
                        'po_buyer',
                        'psd_date',
                    ],
                    'ILIKE',
                    "%$this->search%"
                )
                ->orderBy('created_at', 'DESC')
                ->simplePaginate($this->limitPerPage),
        ]);
    }
}
