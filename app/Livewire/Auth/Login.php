<?php

namespace App\Livewire\Auth;

use Livewire\Component;
use App\Actions\LoginSsoAction;
use Livewire\Attributes\Layout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\Rule;
#[Layout('components.layouts.guest')]
class Login extends Component
{
    #[Rule('required')]
    public string $nik = '';
    #[Rule('required')]
    public string $password = '';

    public function login(LoginSsoAction $loginSsoAction)
    {
        try {
            $this->validate();
            $userApps = $loginSsoAction([
                'nik' => $this->nik,
                'password' => $this->password
            ]);
            Session::put('login_via', 'SSO-AOI');
            Auth::login($userApps);
            $userApps->last_login = now();
            $userApps->save();
            $this->toast('Login Successful');
            return to_route('home');
        } catch (\Exception $e) {
            $this->toast($e->getMessage(), 'error');
        }
    }

    public function render()
    {
        return view('livewire.auth.login');
    }
}
