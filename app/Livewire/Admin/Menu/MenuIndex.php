<?php

namespace App\Livewire\Admin\Menu;

use App\Models\Menu;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;

#[Title('MENU')]
class MenuIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.menu.menu-index', [
            'menus' => Menu::query()
                ->whereAny(
                    [
                        'name'
                    ],
                    'ILIKE',
                    "%$this->search%"
                )
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
