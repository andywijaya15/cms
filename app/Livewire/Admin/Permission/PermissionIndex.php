<?php

namespace App\Livewire\Admin\Permission;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;
use Spatie\Permission\Models\Permission;

#[Title('PERMISSION')]
class PermissionIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.permission.permission-index', [
            'permissions' => Permission::query()
                ->when($this->search, fn ($q) => $q->whereAny(
                    [
                        'name',
                    ],
                    'ILIKE',
                    "%$this->search%"
                ))
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
