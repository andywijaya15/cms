<?php

namespace App\Livewire\Admin\Role;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;
use Spatie\Permission\Models\Role;

#[Title('ROLE')]
class RoleIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.role.role-index', [
            'roles' => Role::query()
                ->when($this->search, fn ($q) => $q->whereAny(
                    [
                        'name',
                    ],
                    'ILIKE',
                    "%$this->search%"
                ))
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
