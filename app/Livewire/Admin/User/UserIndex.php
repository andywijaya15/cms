<?php

namespace App\Livewire\Admin\User;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;

#[Title('USER')]
class UserIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.user.user-index', [
            'users' => User::query()
                ->with('currentRole')
                ->when($this->search, fn ($q) => $q->whereAny(
                    [
                        'nik',
                        'name',
                        'factory_name',
                        'department_name'
                    ],
                    'ILIKE',
                    "%$this->search%"
                ))
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
