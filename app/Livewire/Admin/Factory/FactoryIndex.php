<?php

namespace App\Livewire\Admin\Factory;

use App\Models\Factory;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\Title;

#[Title('FACTORY')]
class FactoryIndex extends Component
{
    use WithPagination;
    public string $search = '';
    public int $limitPerPage = 10;

    public function doSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.factory.factory-index', [
            'factories' => Factory::query()
                ->whereAny(
                    [
                        'name'
                    ],
                    'ILIKE',
                    "%$this->search%"
                )
                ->simplePaginate($this->limitPerPage)
        ]);
    }
}
