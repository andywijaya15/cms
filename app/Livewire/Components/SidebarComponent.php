<?php

namespace App\Livewire\Components;

use App\Models\Menu;
use Livewire\Component;

class SidebarComponent extends Component
{
    public function render()
    {
        return view('livewire.components.sidebar-component', [
            'menus' => Menu::query()
                ->whereNull('parent_id')
                ->when(env('APP_ENV') == 'production', fn ($q) => $q->active())
                ->orderBy('sort')
                ->get()
        ]);
    }
}
