<?php

namespace App\Livewire\Components;

use App\Models\User;
use App\Models\UserLog;
use Livewire\Component;

class NavbarComponent extends Component
{
    public User $user;

    public function getListeners()
    {
        $this->user = auth()->user();
        return [
            "echo:App.Models.User.{$this->user->id},ReloadNotificationEvent" => 'updateList',
        ];
    }

    public function updateList()
    {
    }


    public function readMsg()
    {
        UserLog::query()
            ->where('user_id', auth()->user()->id)
            ->whereNull('read_at')
            ->update([
                'read_at' => now()->toDateTimeString()
            ]);
    }

    public function render()
    {
        return view('livewire.components.navbar-component', [
            'unread_user_logs' => UserLog::query()
                ->where('user_id', auth()->user()->id)
                ->whereNull('read_at')
                ->count() ?? 0
        ]);
    }
}
