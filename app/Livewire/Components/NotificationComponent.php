<?php

namespace App\Livewire\Components;

use App\Models\User;
use App\Models\UserLog;
use Livewire\Component;

class NotificationComponent extends Component
{
    public User $user;

    public function getListeners()
    {
        $this->user = auth()->user();
        return [
            "echo:App.Models.User.{$this->user->id},ReloadNotificationEvent" => 'updateList',
        ];
    }

    public function updateList()
    {
    }

    public function render()
    {
        return view('livewire.components.notification-component', [
            'user_logs' => UserLog::query()
                ->where('user_id', auth()->user()->id)
                ->orderBy('read_at', 'DESC')
                ->latest('created_at')
                ->limit(100)
                ->get()
        ]);
    }
}
