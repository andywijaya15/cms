<?php

namespace App\Actions;

use App\Models\UserLog;
use App\Enums\ProcessNameEnum;
use App\Events\ReloadNotificationEvent;

class CreateUserLogAction
{
    /**
     * Create a new class instance.
     */
    public function __construct()
    {
    }

    /**
     * Invoke the class instance.
     */
    public function __invoke(
        int $userId,
        ProcessNameEnum $processNameEnum,
        string $msg,
        bool $isError = false
    ): void {
        UserLog::query()
            ->create([
                'process_name' => $processNameEnum,
                'user_id' => $userId,
                'msg' => $msg,
                'is_error' => $isError
            ]);
        ReloadNotificationEvent::dispatch($userId);
    }
}
