<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class LoginSsoAction
{
    public string $appUuid;
    public string $appCode;
    public string $appUrl;
    public string $ssoUrl;

    public function __construct()
    {
        $this->appUuid = env('APP_UUID');
        $this->appCode = env('APP_CODE');
        $this->appUrl = env('APP_URL_SSO');
        $this->ssoUrl = env('APP_SSO_ENDPOINT');
    }

    public function __invoke(array $data): User
    {
        ['nik' => $nik, 'password' => $password] = $data;
        if (!$nik) {
            throw new \Exception('nik is required', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        if (!$password) {
            throw new \Exception('password is required', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $tokenSso = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])
            ->post("{$this->ssoUrl}/api/login", [
                'username' => $nik,
                'password' => $password,
            ])
            ->json();

        if (!isset($tokenSso['access_token'])) {
            throw new \Exception($tokenSso['message'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userSso = Http::withToken($tokenSso['access_token'])
            ->withHeaders([
                'Content-Type' => 'application/json',
            ])
            ->get("{$this->ssoUrl}/api/user")
            ->json();
        if (!$userSso) {
            throw new \Exception('User Sso not Found', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userApps = User::query()
            ->whereNik($userSso['nik'])
            ->first();
        if (!$userApps) {
            throw new \Exception('User Not Found in this application', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $userApps;
    }
}
