<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class CreateUserAction
{
    /**
     * Create a new class instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Invoke the class instance.
     */
    public function __invoke(array $data): User
    {
        $role = $data['role'] ?? null;
        $checkUser = User::query()
            ->where('nik', ($data['nik']))
            ->first();
        if ($checkUser) {
            throw new \Exception('User Already Exists', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $userTiket = DB::connection('ticket')
            ->table('users AS u')
            ->join('factory AS f', 'u.factory_id', '=', 'f.id')
            ->where('nik', $data['nik'])
            ->first();

        if (!$userTiket) {
            throw new \Exception('Please Login into tiket first', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = User::query()
            ->create([
                'name' => $userTiket->name,
                'nik' => $userTiket->nik,
                'email' => $userTiket->email,
                'factory_name' => $userTiket->factory_name,
                'department_name' => $userTiket->department_name,
                'subdepartment_name' => $userTiket->subdepartment_name,
                'position' => $userTiket->position,
                'password' => $userTiket->password,
            ]);

        if ($role) {
            $roleId = Role::query()
                ->where('name', $role)
                ->first()
                ->id;
            $user->assignRole($role);
            $user->update([
                'current_role_id' => $roleId,
            ]);
        }
        return $user;
    }
}
