<?php

namespace App\Models;

use App\Models\User;
use App\Enums\IsActiveEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PlanSewingDate extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [];

    protected function casts(): array
    {
        return [
            'is_active' => IsActiveEnum::class,
            'psd_date' => 'date'
        ];
    }

    public static function boot(): void
    {
        parent::boot();
        static::creating(function (Model $model) {
            $model->created_by = auth()->user()->id ?? 1;
            $model->updated_by = auth()->user()->id ?? 1;
        });

        static::updating(function (Model $model) {
            $model->updated_by = auth()->user()->id;
        });
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
