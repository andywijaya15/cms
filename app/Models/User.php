<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'current_role_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function currentRole(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'current_role_id');
    }

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }

    protected function email(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }

    protected function factoryName(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }

    protected function departmentName(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }

    protected function subdepartmentName(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }

    protected function position(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => strtoupper($value ?? ''),
        );
    }
}
