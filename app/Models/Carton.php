<?php

namespace App\Models;

use App\Enums\IsActiveEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Carton extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [];

    protected function casts(): array
    {
        return [
            'is_active' => IsActiveEnum::class,
        ];
    }

    protected function itemCode(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,
        );
    }

    protected function itemDesc(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => strtoupper($value ?? ''),
            set: fn (string|null $value) => $value ? strtoupper($value) : null,
        );
    }

    protected function category(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,
        );
    }

    protected function uom(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,
        );
    }

    protected function color(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,

        );
    }

    protected function upc(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,


        );
    }

    protected function width(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,

        );
    }

    protected function brand(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value ? strtoupper($value) : null,
            set: fn (string|null $value) => $value ? strtoupper($value) : null,

        );
    }
}
