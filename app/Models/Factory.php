<?php

namespace App\Models;

use App\Enums\IsActiveEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Factory extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function casts(): array
    {
        return [
            'is_active' => IsActiveEnum::class,
        ];
    }

    public static function boot(): void
    {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = auth()->user()->id ?? 1;
            $model->updated_by = auth()->user()->id ?? 1;
        });

        static::updating(function ($model) {
            $model->updated_by = auth()->user()->id;
        });
    }
}
