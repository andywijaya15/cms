<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['submenus'];

    public function scopeActive($query): Builder
    {
        return $query->where('is_active', true);
    }

    public function submenus(): HasMany
    {
        return $this->hasMany(Menu::class, 'parent_id')
            ->where('is_active', true)
            ->orderBy('sort', 'asc');
    }

    public function parentmenu(): BelongsTo
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function isAccessible()
    {
        $userPermissions = auth()->user()->currentRole->permissions->pluck('name');

        return $userPermissions->contains(strtolower($this->name));
    }

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => strtoupper($value),
            set: fn (string $value) => strtoupper($value),
        );
    }

    protected function url(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => strtolower($value),
        );
    }
}
