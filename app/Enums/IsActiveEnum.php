<?php

namespace App\Enums;

enum IsActiveEnum: int
{
    case ACTIVE = 1;
    case INACTIVE = 0;

    public function label(): string
    {
        return $this->name;
    }
}
