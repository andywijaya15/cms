<?php

namespace App\Enums;

enum ProcessNameEnum: int
{
    case ALL = 0;
    case UPLOAD_PSD = 1;

    public function label(): string
    {
        return $this->name;
    }
}
