<div>
    <x-card>
        <form wire:submit="uploadPsd">
            <div class="row mb-1">
                <div class="col-md mb-1">
                    <input type="file" wire:model="excel" class="form-control" id="{{ $rand }}">
                </div>
                <div class="col-md-2 mb-1">
                    <div class="d-grid gap-2">
                        <button type="submit" class="btn btn-info">
                            <i class="ph ph-upload-simple me-1"></i>
                            Upload
                        </button>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="d-grid gap-2">
                        <button type="button" wire:click='downloadTemplate' class="btn btn-danger">
                            <i class="ph ph-download-simple me-1"></i>
                            Template
                        </button>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="d-grid gap-2">
                        <button type="button" wire:click='downloadReport' class="btn btn-primary">
                            <i class="ph ph-download-simple me-1"></i>
                            Report
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Po Buyer</th>
                <th>PSD Date</th>
                <th>Upload At</th>
                <th>Upload By</th>
            </x-slot>
            @forelse ($psds as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $psds->firstItem() }}</td>
                    <td>{{ $each->po_buyer }}</td>
                    <td>{{ $each->psd_date }}</td>
                    <td>{{ $each->created_at }}</td>
                    <td>{{ $each->createdBy->name }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $psds->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
