<div class="sidebar sidebar-main sidebar-expand-lg align-self-start sidebar-dark" id="sidebar">
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- Sidebar header -->
        <div class="sidebar-section">
            <div class="sidebar-section-body d-flex justify-content-center">
                <h5 class="sidebar-resize-hide flex-grow-1 my-auto">Menu</h5>

                <div>
                    <button type="button"
                        class="btn btn-light btn-icon btn-sm rounded-pill border-transparent sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
                        <i class="ph-arrows-left-right"></i>
                    </button>

                    <button type="button"
                        class="btn btn-light btn-icon btn-sm rounded-pill border-transparent sidebar-mobile-main-toggle d-lg-none">
                        <i class="ph-x"></i>
                    </button>
                </div>
            </div>
        </div>
        <!-- /sidebar header -->

        <!-- Main navigation -->
        <div class="sidebar-section">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item">
                    <a wire:navigate href="{{ route('home') }}"
                        class="nav-link d-flex align-items-center {{ request()->routeIs('home') ? 'active' : '' }}">
                        <i class="ph-house"></i>
                        <span>
                            HOME
                        </span>
                    </a>
                </li>
                @foreach ($menus as $menu)
                    @if ($menu->isAccessible())
                        <li
                            class="nav-item nav-item-submenu {{ basename(request()->route()->getPrefix()) == $menu->url ? 'nav-item-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="{{ $menu->icon }}"></i>
                                <span>{{ $menu->name }}</span>
                            </a>
                            <ul class="nav-group-sub collapse {{ basename(request()->route()->getPrefix()) == $menu->url ? 'show' : '' }}"
                                data-submenu-title="{{ $menu->name }}">
                                @foreach ($menu->submenus as $submenu)
                                    @if ($submenu->isAccessible())
                                        <li class="nav-item"><a wire:navigate
                                                href="{{ Route::has($submenu->url) ? route($submenu->url) : '#' }}"
                                                class="nav-link {{ request()->routeIs($submenu->url) ? 'active' : '' }}">{{ $submenu->name }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
