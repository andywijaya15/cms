<div class="navbar navbar-dark navbar-expand-lg navbar-static fixed-top">
    <div class="container-fluid">
        <div class="d-flex d-lg-none me-2">
            <button type="button" class="navbar-toggler sidebar-mobile-main-toggle rounded-pill">
                <i class="ph-list"></i>
            </button>
        </div>

        <div class="navbar-brand flex-1 flex-lg-0">
            <a wire:navigate href="{{ route('home') }}" class="d-inline-flex align-items-center">
                <img src="{{ asset('images/carton.png') }}" alt="">
                <span class="d-none d-lg-inline-block mx-lg-2 text-white text-center">CARTON<br>MANAGEMENT SYSTEM</span>
            </a>
        </div>
        <ul class="nav flex-row justify-content-end order-1 order-lg-2">
            <li x-data="{
                currentTime: '',
                currentDate: ''
            }" x-init="setInterval(() => {
                currentTime = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: '2-digit' });
                currentDate = new Date().toLocaleDateString();
            }, 1000)" class="nav-item ms-lg-2 d-flex align-items-center">
                <span x-text="currentDate + ' ' + currentTime"></span>
            </li>

            <li class="nav-item ms-lg-2 d-flex align-items-center">
                <a href="#" class="navbar-nav-link align-items-center navbar-nav-link-icon rounded-pill"
                    data-bs-toggle="offcanvas" data-bs-target="#notifications" wire:click='readMsg'>
                    <i class="ph-bell"></i>
                    <span
                        class="badge bg-yellow text-black position-absolute top-0 end-0 translate-middle-top zindex-1 rounded-pill mt-1 me-1">{{ $unread_user_logs }}</span>
                </a>
            </li>

            <li class="nav-item nav-item-dropdown-lg dropdown ms-lg-2">
                <a href="#" class="navbar-nav-link align-items-center rounded-pill p-1" data-bs-toggle="dropdown">
                    <div class="status-indicator-container">
                        <span class="status-indicator" id="statusConnection"></span>
                    </div>
                    <span class="d-none d-lg-inline-block mx-lg-2">{{ auth()->user()->name }}
                        ({{ strtoupper(auth()->user()->currentRole->name) }})</span>
                </a>

                <div class="dropdown-menu dropdown-menu-end">
                    @if (auth()->user()->roles->count() > 1)
                        @foreach (auth()->user()->roles->where('id', '!=', auth()->user()->current_role_id) as $role)
                            <a href="{{ route('switch.role', $role->id) }}" class="dropdown-item">
                                Switch role to {{ strtoupper($role->name) }}
                            </a>
                        @endforeach
                    @endif
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button class="dropdown-item" type="submit">
                            <i class="ph-sign-out me-2"></i>
                            Logout
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
