<div class="offcanvas offcanvas-end" tabindex="-1" id="notifications">
    <div class="offcanvas-header py-0">
        <h5 class="offcanvas-title py-3">Notification</h5>
        <button type="button" class="btn btn-light btn-sm btn-icon border-transparent rounded-pill"
            data-bs-dismiss="offcanvas">
            <i class="ph-x"></i>
        </button>
    </div>

    <div class="offcanvas-body p-0">
        <div class="p-3">
            <div class="d-flex align-items-start mb-3">
                <div class="flex-1">
                    @foreach ($user_logs as $each)
                        <div class="mb-1">
                            <a href="#" class="{{ $each->is_error ? 'text-danger' : '' }}">
                                {{ $each->msg }}
                            </a>
                            <div class="fs-sm text-muted mt-1">{{ $each->created_at->format('d-M-Y H:i:s') }}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
