<div class="content d-md-flex justify-content-center align-items-center">
    <form wire:submit="login">
        <div class="text-center">
            <img src="{{ asset('images/carton.png') }}" class="h-80px" alt="">
            <h5 class="mt-2 text-warning">CARTON MANAGEMENT SYSTEM</h5>
        </div>
        <div class="text-center mt-2 mb-2">
            <h3 class="mb-1">Sign In</h3>
        </div>
        <div class="mb-1">
            <label class="form-label">NIK</label>
            <div class="form-control-feedback form-control-feedback-start">
                <input type="text" class="form-control" placeholder="NIK" wire:model='nik'>
                <div class="form-control-feedback-icon">
                    <i class="ph-user-circle text-muted"></i>
                </div>
            </div>
        </div>
        <div class="mb-1">
            <label class="form-label">Password</label>
            <div class="form-control-feedback form-control-feedback-start">
                <input type="password" class="form-control" placeholder="DDMMYYYY" wire:model='password'>
                <div class="form-control-feedback-icon">
                    <i class="ph-lock-key text-muted"></i>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <button wire:loading.attr="disabled" type="submit" class="button-custom2 w-25">Sign In</button>
        </div>
    </form>
</div>
