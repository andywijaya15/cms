<div>
    <x-card color='dark'>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Name</th>
                <th>NIK</th>
                <th>Factory</th>
                <th>Departement</th>
                <th>Role</th>
                <th>Last Login</th>
            </x-slot>
            @forelse ($users as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $users->firstItem() }}</td>
                    <td>{{ $each->name }}</td>
                    <td>{{ $each->nik }}</td>
                    <td>{{ $each->factory_name }}</td>
                    <td>{{ $each->department_name }}</td>
                    <td>{{ $each->currentRole->name }}</td>
                    <td>{{ $each->last_login }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $users->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
