<div>
    <x-card color='dark'>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Name</th>
            </x-slot>
            @forelse ($roles as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $roles->firstItem() }}</td>
                    <td>{{ $each->name }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="2" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $roles->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
