<div>
    <x-card color='dark'>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Name</th>
                <th>Status</th>
            </x-slot>
            @forelse ($factories as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $factories->firstItem() }}</td>
                    <td>{{ $each->name }}</td>
                    <td>{{ $each->is_active->label() }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $factories->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
