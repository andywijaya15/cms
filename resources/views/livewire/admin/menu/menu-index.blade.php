<div>
    <x-card>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Name</th>
                <th>Url</th>
            </x-slot>
            @forelse ($menus as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $menus->firstItem() }}</td>
                    <td>{{ $each->name }}</td>
                    <td>{{ $each->url }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $menus->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
