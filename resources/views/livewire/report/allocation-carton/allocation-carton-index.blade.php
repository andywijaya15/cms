<div>
    <x-card>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Plan Ref Number</th>
                <th>Po Buyer</th>
                <th>Item Code</th>
                <th>Need</th>
                <th>Created At</th>
            </x-slot>
            @forelse ($allocation_cartons as $each)
                <tr wire:key='{{ $each->ref_id }}'>
                    <td>{{ $loop->index + $allocation_cartons->firstItem() }}</td>
                    <td>{{ $each->plan_ref_number }}</td>
                    <td>{{ $each->po_buyers }}</td>
                    <td>{{ $each->item_code }}</td>
                    <td>{{ $each->total }}</td>
                    <td>{{ $each->created_at }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="6" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $allocation_cartons->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
