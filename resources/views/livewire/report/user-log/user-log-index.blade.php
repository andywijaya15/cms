<div>
    <x-card>
        <div class="row mb-1">
            <div class="col-md">
                <select class="form-control text-uppercase text-center" wire:model.live='processNameEnum'>
                    @foreach ($process_names as $each)
                        <option value="{{ $each->value }}">{{ $each->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Process Name</th>
                <th>Message</th>
                <th>Happen At</th>
                <th>Status</th>
            </x-slot>
            @forelse ($user_logs as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $user_logs->firstItem() }}</td>
                    <td>{{ $each->item_id }}</td>
                    <td>{{ $each->item_code }}</td>
                    <td>{{ $each->width }}</td>
                    <td>{{ $each->is_active->label() }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $user_logs->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
