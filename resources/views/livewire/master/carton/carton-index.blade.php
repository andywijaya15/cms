<div>
    <x-card>
        <x-table>
            <x-slot name="thead">
                <th>#</th>
                <th>Product Id ERP</th>
                <th>Product Name</th>
                <th>Width</th>
                <th>Status</th>
            </x-slot>
            @forelse ($cartons as $each)
                <tr wire:key='{{ $each->id }}'>
                    <td>{{ $loop->index + $cartons->firstItem() }}</td>
                    <td>{{ $each->item_id }}</td>
                    <td>{{ $each->item_code }}</td>
                    <td>{{ $each->width }}</td>
                    <td>{{ $each->is_active->label() }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        There is no data
                    </td>
                </tr>
            @endforelse
        </x-table>
        {{ $cartons->links(data: ['scrollTo' => false]) }}
    </x-card>
</div>
