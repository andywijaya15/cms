<div>
    <form wire:submit="doSearch">
        <div class="row mb-1">
            <div class="col-md mb-1">
                <input wire:model='search' class="form-control form-control-sm text-uppercase" type="text"
                    autocomplete="off" placeholder="Search Here...">
            </div>
            <div class="col-md-2 mb-1">
                <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
            </div>
        </div>
    </form>
    <div class="row mb-1">
        <div class="table-responsive">
            <table class="table datatable-basic table-borderless table-hover table-striped table-xs text-xs"
                width="100%">
                <thead class="bg-info text-white text-uppercase">
                    {{ $thead }}
                </thead>
                <tbody>
                    {{ $slot }}
                </tbody>
            </table>
        </div>
    </div>
</div>
