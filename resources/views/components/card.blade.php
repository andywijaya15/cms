<div class="card">
    <div class="card-header bg-dark">
        <h6 class="mb-0"></h6>
    </div>
    <div class="card-body">
        {{ $slot }}
    </div>
</div>
