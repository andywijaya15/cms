<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('images/carton.png') }}">
    <link rel="icon" href="{{ asset('images/carton.png') }}">
    <title>CMS</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        .button-custom2 {
            font-size: 14px;
            color: #fff;
            line-height: 1.2;
            border: none;
            margin-top: 2em;
            margin-bottom: 2em;

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 20px;
            min-width: 160px;
            height: 42px;
            border-radius: 7px;

            background: #FB6514;
            position: relative;
            z-index: 1;

            -webkit-transition: all 0.2s;
            -o-transition: all 0.2s;
            -moz-transition: all 0.2s;
            transition: all 0.2s;
        }

        .button-custom2:hover {
            background: rgb(242, 119, 0);

            -webkit-transition: all 0.2s;
            -o-transition: all 0.2s;
            -moz-transition: all 0.2s;
            transition: all 0.2s;
        }

        .bg-img {
            width: 100%;
            min-height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            padding: 15px;

            background-image: url('../../images/background-home.jpg');
            background-color: #F9FAFB;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            position: absolute;
            opacity: 1;
            z-index: -1;
        }

        .box {
            padding: 1%;
            margin: auto;
            background-color: rgba(255, 255, 255, 0.9);
            border-radius: 15px;
        }

        .box-content {
            opacity: 1;
        }

        @media (max-width: 768px) {
            .box {
                width: 100%;
            }
        }

        @media (min-width: 769px) {
            .box {
                width: 50%;
            }
        }
    </style>
</head>

<body>
    <!-- Main navbar -->
    <div class="navbar navbar-sm navbar-expand-lg" style="background: transparent !important">
        <div class="container-fluid">
            <div class="d-flex d-lg-none me-2">

            </div>
            <div class="d-inline-flex flex-1 flex-lg-0">
                <a href="#" class="navbar-brand d-inline-flex align-items-center">
                    <img src="{{ asset('images/bbi.svg') }}" style="width:100% !important;height:100% !important">
                </a>
            </div>
            <div class="flex-row">

            </div>
            <div class="navbar-collapse justify-content-center flex-lg-1 order-2 order-lg-1 collapse"
                id="navbar-mobile">

            </div>
            <div class="flex-row justify-content-end order-1 d-inline-flex flex-1 flex-lg-0 order-lg-2 text-end">
                <img src="{{ asset('images/aoi.svg') }}" style="width:100% !important">
            </div>
        </div>
    </div>
    <div class="bg-img">

    </div>
    <div class="page-content justify-content-center align-items-center">
        <div class="content-wrapper">
            <div class="content-inner">
                <div class="box">
                    <div class="box-content">
                        {{ $slot }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
