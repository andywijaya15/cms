<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ isset($title) ? "$title - CMS" : 'CMS' }}</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="navbar-top">
    <livewire:components.navbar-component />
    <div class="page-content pt-0 mt-3">
        <livewire:components.sidebar-component />
        <div class="content-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-md">
                        {{ $slot }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <div class="navbar navbar-sm navbar-footer border-top">
        <div class="container-fluid">
            <span>&copy; {{ date('Y') }} - <a href="#">CMS</a></span>

            <ul class="nav">
                <li class="nav-item">
                    <a href="https://ticket.bbi-apparel.com/my-ticket"
                        class="navbar-nav-link navbar-nav-link-icon rounded" target="_blank">
                        <div class="d-flex align-items-center mx-md-1">
                            <i class="ph-lifebuoy"></i>
                            <span class="d-none d-md-inline-block ms-2 text-danger">Found Issue ? Click me</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <livewire:components.notification-component lazy />
</body>

</html>
