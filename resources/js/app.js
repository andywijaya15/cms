import "./bootstrap";
import "./limitless_app";
import "../css/all.min.css";
import "@fontsource/inter";
import "phosphor-icons";
import * as bootstrap from "bootstrap";
window.bootstrap = bootstrap;
import Swal from "sweetalert2/dist/sweetalert2.js";
window.Swal = Swal;
