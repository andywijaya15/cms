<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Actions\CreateUserAction;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(CreateUserAction $createUserAction): void
    {
        // ict
        $niks = [
            '221000001',
            '190800027',
            '230500011',
            '191000049',
            '170901001',
            '220800197',
            '220900005',
            '230300014',
            '240100162',
            '160209680',
            '180200167'
        ];

        $adminRole = Role::query()
            ->where('name', 'admin')
            ->first();

        foreach ($niks as $nik) {
            $createdUser = $createUserAction([
                'nik' => $nik,
                'role' => 'admin',
            ]);
            $createdUser->assignRole($adminRole->name);
        }
    }
}
