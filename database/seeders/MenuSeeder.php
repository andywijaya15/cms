<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = Menu::query()
            ->create([
                'name' => strtoupper('admin'),
                'url' => strtolower('admin'),
                'sort' => 0,
                'icon' => 'ph-user',
            ]);
        $master = Menu::query()
            ->create([
                'name' => strtoupper('master'),
                'url' => strtolower('master'),
                'sort' => 0,
                'icon' => 'ph-database',
            ]);
        $transaction = Menu::query()
            ->create([
                'name' => strtoupper('transaction'),
                'url' => strtolower('transaction'),
                'sort' => 0,
                'icon' => 'ph-swap',
            ]);
        $report = Menu::query()
            ->create([
                'name' => strtoupper('report'),
                'url' => strtolower('report'),
                'sort' => 0,
                'icon' => 'ph-microsoft-excel-logo',
            ]);
        $menus = [
            // admin
            [
                'name' => strtoupper('Menu'),
                'url' => strtolower('admin.menu'),
                'parent_id' => $admin->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => strtoupper('User'),
                'url' => strtolower('admin.user'),
                'parent_id' => $admin->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => strtoupper('Role'),
                'url' => strtolower('admin.role'),
                'parent_id' => $admin->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => strtoupper('Permission'),
                'url' => strtolower('admin.permission'),
                'parent_id' => $admin->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => strtoupper('Factory'),
                'url' => strtolower('admin.factory'),
                'parent_id' => $admin->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // master
            [
                'name' => strtoupper('Carton'),
                'url' => strtolower('master.carton'),
                'parent_id' => $master->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // transaction
            [
                'name' => strtoupper('PSD'),
                'url' => strtolower('transaction.psd'),
                'parent_id' => $transaction->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // report
            [
                'name' => strtoupper('Allocation Carton'),
                'url' => strtolower('report.allocation-carton'),
                'parent_id' => $report->id,
                'sort' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => strtoupper('User Log'),
                'url' => strtolower('report.user-log'),
                'parent_id' => $report->id,
                'sort' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
        Menu::query()
            ->insert($menus);
    }
}
