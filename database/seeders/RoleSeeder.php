<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            'admin',
        ];

        foreach ($roles as $r) {
            Role::query()
                ->create(['name' => $r]);
        }

        Role::query()
            ->where('name', 'admin')->first()
            ->givePermissionTo(Permission::all());
    }
}
