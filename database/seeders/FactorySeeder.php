<?php

namespace Database\Seeders;

use App\Models\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $factories = [
            'AOI 1',
            'AOI 2',
            'AOI 3',
            'AOI 3 EXT',
        ];

        foreach ($factories as $each) {
            Factory::query()
                ->create([
                    'name' => $each,
                ]);
        }
    }
}
