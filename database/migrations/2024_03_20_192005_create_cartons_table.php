<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cartons', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->bigInteger('item_id')->unique();
            $table->string('item_code');
            $table->text('item_desc')->nullable();
            $table->string('category');
            $table->string('uom');
            $table->string('color')->nullable();
            $table->string('upc')->nullable();
            $table->string('width')->nullable();
            $table->boolean('is_recycle');
            $table->string('brand')->nullable();
            $table->boolean('is_active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cartons');
    }
};
