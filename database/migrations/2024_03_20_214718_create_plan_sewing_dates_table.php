<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plan_sewing_dates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignId('created_by')->references('id')->on('users')->constrained()->cascadeOnUpdate();
            $table->foreignId('updated_by')->references('id')->on('users')->constrained()->cascadeOnUpdate();
            $table->timestamps();
            $table->string('po_buyer');
            $table->date('psd_date');
            $table->boolean('is_active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plan_sewing_dates');
    }
};
