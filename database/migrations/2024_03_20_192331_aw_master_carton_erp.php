<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
        CREATE OR REPLACE VIEW aw_master_carton_erp AS
        SELECT
            *
        FROM
            dblink(
                'dbname=aimsdbc port=5432 host=192.168.51.202 user=adempiere password=Becarefulwithme' :: text,
                'SELECT 
                    mp.m_product_id AS item_id, 
                    UPPER(mp.value) AS item_code, 
                    UPPER(btrim(regexp_replace(mp.description::text, ''\s+''::text, '' ''::text, ''g''::text))) AS item_desc, 
                    UPPER(mp.kst_brand) AS brand,
                    UPPER(btrim(mpc.value::text)) AS category, 
                    UPPER(cu.uomsymbol) AS uom, 
                    UPPER(cl.name) AS color, 
                    UPPER(coalesce(mp.upc, ''-''::character varying)) AS upc, 
                    UPPER(wd.value) AS width, 
                    UPPER(mp.producttype) AS item_type, 
                    mp.recycle AS is_recycle, 
                    DATE(mp.created) AS created_at,
                    DATE(mp.updated) AS updated_at 
                FROM m_product AS mp 
                INNER JOIN c_uom AS cu ON cu.c_uom_id = mp.c_uom_id 
                INNER JOIN m_product_category AS mpc ON mpc.m_product_category_id = mp.m_product_category_id 
                LEFT JOIN kst_colordetails AS cl ON cl.kst_colordetails_id = mp.kst_colordetails_id 
                LEFT JOIN kst_width AS wd ON wd.kst_width_id = mp.kst_width_id 
                WHERE mpc.m_product_category_id = 1000048 
                    AND mp.isactive = ''Y'' 
                    AND mp.discontinued = ''N'''
            ) AS product_carton(
                item_id BIGINT,
                item_code VARCHAR,
                item_desc TEXT,
                brand VARCHAR,
                category VARCHAR,
                uom VARCHAR,
                color VARCHAR,
                upc VARCHAR,
                width VARCHAR,
                item_type VARCHAR,
                is_recycle BOOLEAN,
                created_at DATE,
                updated_at DATE
            );
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS aw_master_carton_erp');
    }
};
