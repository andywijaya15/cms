<?php

use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('PSD', function () {
    return;
});

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
