<?php

use App\Events\PsdCreated;
use App\Livewire\Auth\Login;
use App\Livewire\Home\HomeIndex;
use Illuminate\Support\Facades\Route;
use App\Livewire\Admin\Menu\MenuIndex;
use App\Livewire\Admin\Role\RoleIndex;
use App\Livewire\Admin\User\UserIndex;
use App\Livewire\Transaction\Psd\PsdIndex;
use App\Livewire\Master\Carton\CartonIndex;
use App\Livewire\Admin\Factory\FactoryIndex;
use App\Livewire\Report\UserLog\UserLogIndex;
use App\Http\Controllers\Auth\LogoutController;
use App\Livewire\Admin\Permission\PermissionIndex;
use App\Livewire\Report\AllocationCarton\AllocationCartonIndex;

Route::redirect('/', 'home');

Route::middleware('guest')->group(function () {
    Route::get('login', Login::class)->name('login');
});

Route::middleware('auth')->group(function () {
    Route::get('home', HomeIndex::class)->name('home');
    Route::post('logout', LogoutController::class)->name('logout');

    Route::prefix('admin')->middleware('permission:admin')->group(function () {
        Route::middleware(['permission:menu'])->group(function () {
            Route::get('menu', MenuIndex::class)->name('admin.menu');
        });
        Route::middleware(['permission:factory'])->group(function () {
            Route::get('factory', FactoryIndex::class)->name('admin.factory');
        });
        Route::middleware(['permission:user'])->group(function () {
            Route::get('user', UserIndex::class)->name('admin.user');
        });
        Route::middleware(['permission:role'])->group(function () {
            Route::get('role', RoleIndex::class)->name('admin.role');
        });
        Route::middleware(['permission:permission'])->group(function () {
            Route::get('permission', PermissionIndex::class)->name('admin.permission');
        });
    });

    Route::prefix('master')->middleware('permission:master')->group(function () {
        Route::middleware(['permission:carton'])->group(function () {
            Route::get('carton', CartonIndex::class)->name('master.carton');
        });
    });

    Route::prefix('transaction')->middleware('permission:transaction')->group(function () {
        Route::middleware(['permission:psd'])->group(function () {
            Route::get('psd', PsdIndex::class)->name('transaction.psd');
        });
    });

    Route::prefix('report')->middleware('permission:report')->group(function () {
        Route::middleware(['permission:allocation carton'])->group(function () {
            Route::get('allocation-carton', AllocationCartonIndex::class)->name('report.allocation-carton');
        });
        Route::middleware(['permission:user log'])->group(function () {
            Route::get('user-log', UserLogIndex::class)->name('report.user-log');
        });
    });
});
